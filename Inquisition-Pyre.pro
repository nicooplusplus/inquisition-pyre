#-------------------------------------------------
#
# Project created by QtCreator 2016-02-18T22:08:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Inquisition-Pyre
TEMPLATE = app


SOURCES  += main.cpp

HEADERS  +=

FORMS    +=

QMAKE_CXXFLAGS += -std=c++11

unix:!macx: LIBS += -lopendht

unix:!macx: LIBS += -lmsgpackc

unix:!macx: LIBS += -lgnutls
