/*
 * main.cpp
 *
 * Copyright 2016 Jäger Nicolas <jagernicolas@legtux.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHnodesFiles ANY WARRANTY; withnodesFiles even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include <vector>
#include <iostream>

#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QFile>
#include <QDataStream>

#include <opendht.h>
//#include <opendht/dht.h>

#include <QDebug>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QClipboard* clipboard = a.clipboard();

    QFile file("nodes.dat");

    if ( !file.open(QIODevice::ReadWrite) )
    {
        qCritical() << "file error!";
    }

//    while (!file.atEnd())
//    {
//       dht::NodeExport node;

//    }

    QDataStream nodesFiles(&file);

    dht::DhtRunner node;

    /// all computer can be used as bootstrap
    node.run( 4222, dht::crypto::generateIdentity(), true, true );

    /// todo : load some table with nodes connected in the past. If no known node, Use a node set by
    /// the user


    a.connect(clipboard, &QClipboard::changed, [&](QClipboard::Mode mode){
        const QMimeData *mimeData = clipboard->mimeData();
        if ( mimeData->hasText() )
        {
            std::string str = clipboard->text( mode ).toStdString();
            node.put( "key", str );
            /// then send the data to others nodes

            /// this part is used for covenience, will be deleted when daemonized
            if(str=="exit")
                a.exit();
        }
    });

    /// need some opendht signal when data come from some node

    a.exec();

    std::vector<dht::NodeExport> nodes(node.exportNodes());

    foreach (dht::NodeExport node, nodes)
    {
        msgpack::sbuffer sbuf;
        msgpack::pack(sbuf, node.id);
//        msgpack::pack(sbuf, node.ss);
        msgpack::pack(sbuf, node.sslen);
//        std::string id( node.id.toString() );
//        std::string addr( dht::print_addr(node.ss,node.sslen ) );
//        nodesFiles << QString::fromStdString( id ) << QString::fromStdString( addr );

    }

    file.flush();

    file.close();

    qDebug() << "leaving";

    node.join();

    return EXIT_SUCCESS;
}
